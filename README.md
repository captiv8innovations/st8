st8 (state)
===================
st8 is a jQuery class that generates dynamic and beautiful dialog boxes with an overlay background container.

## Terms of Use
cr8 is created and maintained by [captiv8 Innovations Inc](https://captiv8innovations.com). The code is available under the [captiv8 Terms of Use License](https://captiv8innovations.com/terms).