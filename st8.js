var st8 = function(){
    var _this = this;
    _this.id = guid();
    this.createDialog = function(params){
        if (typeof(params) === "object") {
            var hasErrors = false;
            var requiredParams = Array('title', 'message');
            for (var i = requiredParams.length - 1; i >= 0; i--) {
                if (params.hasOwnProperty(requiredParams[i]) === false) {
                    console.error("sl8: " + requiredParams[i] + " parameter of type string not set using sl8.createDialog({ "+ requiredParams[i] +" : \"string\" }) property.");
                    hasErrors = true;
                }
            }
            var optionalParams = Array('buttons');
            var defaultOptionalParams = {
                'buttons' : [{
                    name : "OK",
                    action : "$(\"#sl8-dialog-"+_this.id +", #sl8-overlay-"+_this.id + "\").remove();"
                }]
            };
            for (i = optionalParams.length - 1; i >= 0; i--) {
                if (params.hasOwnProperty(optionalParams[i]) === false) {
                    var optionalParamsKey = optionalParams[i];
                    params[optionalParamsKey] = defaultOptionalParams[optionalParamsKey];
                    console.log(params);
                }
            }
            var options = Array('accentColor');
            var defaultOptions = {
                accentColor : "#34AFCB"
            };
            for (i = optionalParams.length - 1; i >= 0; i--) {
                if ((params.hasOwnProperty("options") &&  params.options.hasOwnProperty(options[i])) === false) {
                    var optionsKey = options[i];
                    params.options[optionsKey] = defaultOptions[optionsKey];
                    console.log(params);
                }
            }
            if (hasErrors === true) {
                return false;
            } else {
                //console.log($(document.body.innerHTML));
                $('body')
                .append(
                    $("<div/>", {
                        "class":"sl8-overlay",
                        "id":"sl8-overlay-"+_this.id
                    }).css({
                        background: "black",
                        position: "fixed",
                        font: "inherit",
                        fontWeight: "inherit",
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        "z-index": 999,
                        opacity: 0
                    })
                ).append(
                    $("<div/>", {
                        "class": "sl8-dialog",
                        "id": "sl8-dialog-"+_this.id
                    }).css({
                        position: 'fixed',
                        top: "50%",
                        left: "50%",
                        width: "400px",
                        maxWidth: "500px",
                        minHeight: "100px",
                        padding: "50px 50px 40px 50px",
                        background: "white",
                        font: "inherit",
                        fontWeight: "inherit",
                        "z-index": 1000
                    }).append(
                        $("<div/>", {
                            "class": "accent"
                        }).css({
                            position: "absolute",
                            top:0,
                            left: 0,
                            right: 0,
                            height: "5px",
                            background: params.options.accentColor,
                            width: "100%"
                        })
                    ).append(
                        $("<div/>", {
                            "class": "title"
                        }).css({
                            fontSize: "200%",
                            padding: "0px 0 10px 0"
                        }).text(params.title)
                    ).append(
                        $("<div/>", {
                            "class": "message"
                        }).css({
                            fontSize: "100%",
                            padding: "10px 0 30px 0px"
                        }).text(params.message)
                    ).append(
                        $("<div/>", {
                            "class": "buttons"
                        }).css({
                            height: "50px",
                            font: "inherit",
                            fontWeight: "inherit",
                            textAlign: "center"
                        }).append(
                            $("<button/>", {
                                "class":"golden",
                                "onclick":params.buttons[0].action
                            }).css({
                                border: "2px solid white",
                                padding: "15px 50px",
                                color: "white",
                                backgroundColor: "transparent",
                                font: "inherit",
                                "text-transform" : "uppercase",
                                cursor: "pointer",
                                background: params.options.accentColor,
                                "-webkit-transition": "all ease-out 200ms",
                                "-moz-transition": "all ease-out 200ms",
                                "-o-transition": "all ease-out 200ms",
                                transition: "all ease-out 200ms"
                            }).text(params.buttons[0].name)
                        )
                    )
                );

                $("div#sl8-dialog-"+_this.id).css({
                    marginLeft: -($("div#sl8-dialog-"+_this.id).outerWidth()/2),
                    marginTop: -($("div#sl8-dialog-"+_this.id).outerHeight()/2) - 30,
                    opacity: 0
                });

                $("div#sl8-overlay-"+_this.id).animate({
                    opacity : 0.7
                });

                $("div#sl8-dialog-"+_this.id).animate({
                    top : "+=30",
                    opacity : 1
                });
                console.log($(document).innerHeight()  );
            }
        } else {
            console.error("st8: Unexpected type \"" + typeof response + "\" found instead of type object when initalizing st8.createDialog(params)");
        }
    };

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
          }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};